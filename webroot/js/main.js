// JavaScript Document

(function($) {
	
	$(document).ready(function(e) {
		set_lang();
		//statistics();
		getPosts();
		tabs();
		//ZoomView();
		//ZoomViewSupprt();
		//$('.tooltip').tooltip();
		
		
		$("#icon_category").click(function(){
			$("#btmCategory").show(600);
		});
		$(".Category h2").click(function(){
			$("#btmCategory").hide(600);
		});
		
		$("#toTop").click(function () {
			   $("html, body").animate({scrollTop: 0}, 1000);
			});
		
		if($("#horizontalmenu").length){
		$('#horizontalmenu').ddscrollSpy({ // initialize first demo
			scrolltopoffset: -165
		});
		}
		
		$("#mCate").click(function () {
			   $("html, body").animate({scrollTop: 0}, 1000);
			   $("#mMenuItem").toggle(500);
		});
		
		$("#mSearch").click(function () {
			   $("html, body").animate({scrollTop: 0}, 1000);
			   $(".mform-search").toggle(500);
		});
		
		
		$(window).scroll(function() {
			var stop = $("body").scrollTop();
			if(stop > 68){
				$("#Header").addClass("HeaderFixed");
			}
			else{
				$("#Header").removeClass("HeaderFixed");
			}
			
		});
		
		/*
		$('#regionsearch').autocomplete({
			source : '?mod=product&site=search_region',
			minLength : 1,	
		});
     	 
	*/
			
	var availableTags = [
	"ActionScript",
	"AppleScript",
	"Asp",
	"BASIC",
	"C",
	"C++",
	"Clojure",
	"COBOL",
	"ColdFusion",
	"Erlang",
	"Fortran",
	"Groovy",
	"Haskell",
	"Java",
	"JavaScript",
	"Lisp",
	"Perl",
	"PHP",
	"Python",
	"Ruby",
	"Scala",
	"Scheme"
	];
$( "#regionsearch" ).autocomplete({
source: availableTags
});

		
		
	   $(".i-group-item").click(function(){
	 $(this).parent().find(".show-group-item").toggle("slow");
	
	   });
			
   
   jQuery(document).ready(function() {
	    jQuery('.tabs .tab-links a').on('click', function(e)  {
	        var currentAttrValue = jQuery(this).attr('href');
	 
	        // Show/Hide Tabs
	        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
	 
	        // Change/remove current tab to active
	        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
	 
	        e.preventDefault();
	    });
	});
		
		if($('#slide').length){
			
			$('#slide').nivoSlider({
				pauseTime: 2000,
				directionNav: true
			});
		}
		
		 $('.hide-box h3').click(function() {
             $(this).parent().find('ul').toggle("slow");
         });
		
		if($("#FrmValidate").length){
			$("#FrmValidate").validate();   
		}
		
		setInterval(function(){
			$("#ProductViewList").load("?mod=product&site=load_product_view_list");
		}, 3000);
		
		load_product(".load_product_hot", "load_product_hot");
		load_product(".load_product_new", "load_product_new");
		load_product(".load_product_promotions", "load_product_promotions");
		
		
		
		
		$("#posts_price_active").change(function(){
			var price = $(this).val();
			$("#posts_price_active_gold").html(price);
		});
		
		$(".img_delete").click(function(){
			$(this).parent("div").parent("div").parent("div").hide(1000);
			var img = $(this).parent("div").prev("img").attr("src");
			$.post('?mod=helps&site=file_delete',{
				file: img
			}).done(function(){
			});
			return false;
		});
		
		var price_select_val = $("#price_select").val();
		if(price_select_val==0){
			$("#add_price").hide().val(null);
		}
		else{
			$("#add_price").show();
		}
		$("#price_select").change(function(){
			var id = $(this).val();
			if(id==0){
				$("#add_price").hide().val(null);
			}
			else{
				$("#add_price").show();
			}
		});
		
		$(".show_edit").click(function(){
			$("#admin .frm_admin").slideUp(500);
			$(this).parent().next().slideDown(1000);
		});
		
		$(".video_list").click(function(){
			var link = $(this).attr('link');
			$("#show_video").attr("src", link);
		});

	});
	
	
	

	
	function load_product(click_item, link_load){
		$(click_item).click(function(){
			var page = $(this).attr('page');
			var next = parseInt(page)+1;
			$(click_item).attr("page", next);
			$("#load"+page).load("?mod=helps&site="+link_load+"&page="+page);
		});

	}
	
	/*
	function ZoomView() {
		$('.ZoomViewMap').click(function(){
			var id = $(this).attr("data-id");
			$(".ZoomView").show();
			$(".Zoom").show();
			$('.ZoomView').html('<p style="text-align:center"><img src="webroot/images/img/loading45.gif" width="200" style="margin: 30px auto"/></p>');
			$(".ZoomView").load("?mod=helps&site=load_map&id=" + id);
		});
		$('.Zoom .Close').live('click', function(){
			$(".Zoom").hide();
			$(".ZoomView").hide();
		});
	}
	
	function ZoomViewSupprt() {
		$('.ZoomViewSupport').click(function(){
			$(".Zoom_support").show();
			//$('#Support').html('<p style="text-align:center"><img src="webroot/images/img/loading45.gif" width="200" style="margin: 30px auto"/></p>');
			$("#Support").show();
		});
		$('.Zoom_support .Close').live('click', function(){
			$(".Zoom_support").hide();
			$("#Support").hide();
		});
	}
	*/
	
	/*
	 * Xử lý thanh sidebar
	 * Tự động chạy xuống khi cuộn
	 */
	function add_padding(movable, movlimit) {
        var stop = $(this).scrollTop();
        var padd = $(this).scrollTop() - $(movable).offset().top + 10;
        var he = $(movable).height();
        var limit = $(movlimit).height();
        if (padd >= 0) {
            if (he + padd <= limit) {
                $(movable).stop().animate({ "padding-top": padd }, 800);
            } else {
                $(movable).stop().animate({ "padding-top": limit - he }, 800);
            }
        } else {
            $(movable).stop().animate({ "padding-top": 0 }, 800);
        }
	}
	
	/*
	 * Xử lý quảng cáo hai bên
	 * Tự động hiện/ẩn khi thay đổi kích cấu hình màn hình
	 */
	function handle_ad(){
		var w = $(document).width();
		if(w > 1330){
			 $('#ad_1').show();
			 $('#ad_2').show();
		}
		else if(w > 1260){
			 $('#ad_1').show();
			 $('#ad_2').show();
		 }
		else{
			 $('#ad_1').hide();
			 $('#ad_2').hide();
		}
	}
	
	/*
	 * Gọi thống kê truy cập
	 */
	function statistics(){
		$("#statistics").load("?mod=helps&site=statistics");
	}
	
	/*
	 * Khởi tạo ngôn ngữ sử dụng trên website
	 */
	function set_lang(){
		$("#languages a").click(function(){
			var id = $(this).attr('data-id');
			$.post('?mod=helps&site=set_lang&id='+id).done(function(){
				location.reload()
			});
		});
	}
	
	/*
	 * Active menu
	 */
	function active_menu(){
		$(".itemMenu").click(function(){
			var id = $(this).attr("data");
			var link = $(this).attr("link");
			$.post('?mod=helps&site=active_menu&id='+id).done(function(){
				//location.reload()
				window.location = link;
			});
		});
	}
	

	function video(){
		$("#list_video li").first().children("a").addClass("active");
		var first = $("#list_video li").first().children("a").attr("link");
		$("#show_video").attr("src", first);
		
		$("#list_video a").click(function(){
			$("#list_video a").removeClass("active");
			$(this).addClass("active");
			var link = $(this).attr("link");
			$("#show_video").attr("src", link);
		});
	}
	

	
	/*
	 * Mục hỏi đáp
	 */
	function question(){
		$(".answer").hide();
		$(".question a").click(function(){
			$(".answer").slideUp();
			$(this).next().slideDown();
		});
		$(".question").first().children("div.answer").show();
	}
	
	function tabs(){
		$('[id^=tab-]').hide();
		$('[id^=tab-]').first().show();
		$('.tabs li').first().addClass("active");
		$('.tabs li').first().next().addClass('tnext');
		$('.tabs li').click(function(){
			var id = $(this).attr("tab");
			$('.tabs li.tprev').removeClass('tprev');
			$('.tabs li.active').removeClass('active');
			$('.tabs li.tnext').removeClass('tnext');
			
			$(this).prev().addClass('tprev');
			$(this).addClass('active');
			$(this).next().addClass('tnext');
			$('[id^=tab-]').hide();
			$('#tab-'+id).show();
		});
	}
	
	
	/*
	 * Gọi các xử lý bất động sản
	 */
	function getPosts(){
		
		$("#province li a").click(function(){
			var province = $(this).attr("id");
			if(province=="all"){
				$("#show_local").slideToggle();
			}
			/*
			else{
				$.post('?mod=helps&site=search_posts',{
					submit: 1, province: province
				}).done(function(){
					location.reload()
				});
				return false;
			}
			*/
		});
		
		$("#category_id").change(function(){
			var id = $(this).val();
			$("#category_detail").load("?mod=helps&site=bds_category&id="+id);
		});

		$("#category_id").change(function(){
			var id = $(this).val();
			$("#category_type").load("?mod=helps&site=bds_category_type&id="+id);
		});

		// Tinh thanh
		$("#local_province").change(function(){
			var id = $(this).val();
			$("#local_district").load("?mod=helps&site=bds_local&id="+id);
		});
		$("#local_district").change(function(){
			var id = $(this).val();
			$("#local_ward").load("?mod=helps&site=bds_local&id="+id);
		});
		$("#local_ward").change(function(){
			var id = $(this).val();
			$("#local_street").load("?mod=helps&site=bds_local&id="+id);
		});
		
		// Xử lý tìm kiếm
		$("#local_search").click(function(){
			var province = $("#local_province").val();
			var district = $("#local_district").val();
			var ward = $("#local_ward").val();
			var street = $("#local_street").val();
			$.post('?mod=helps&site=search_posts',{
				submit: 1, province: province, district: district, ward: ward, street: street
			}).done(function(){
				location.reload()
			});
			return false;
		});
		

	}
	
	
	function scroll_to_show(){
		var h_header = parseInt($("#Header").height());
		var h_home = parseInt($("#Home").height());
		var height = h_header + h_home;
		var stop = $("body").scrollTop();
		if(stop > height){
			$("#icon_category").fadeIn();
		}
		else{
			$("#icon_category").fadeOut();
			$("#btmCategory").fadeOut();
		}
	}

	
	$(window).scroll(function() {
		//add_padding('.movable', '.movlimit');
		//add_padding('.movable2', '.movlimit');
	    if ($(this).scrollTop()) {
	        $('#toTop').fadeIn();
	    } else {
	        $('#toTop').fadeOut();
	    }
	    
	    scroll_to_show();
	});


	// Xu ly khi thay doi kich thuoc man hinh
	$(window).resize(function() {
		var w = $(document).width();
		if(w > 1330){
			 $('#ad_1').show().removeClass('add_sml add_sml_left');
			 $('#ad_2').show().removeClass('add_sml add_sml_right');
		}
		else if(w > 1260){
			 $('#ad_1').show().addClass('add_sml add_sml_left');
			 $('#ad_2').show().addClass('add_sml add_sml_right');
		 }
		else{
			 $('#ad_1').hide();
			 $('#ad_2').hide();
		}

	});

}(jQuery));
