<meta charset="utf-8">
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
<meta http-equiv="content-language" content="vi" />
<base href="{$domain}"></base>
<title>{if $seo.title neq NULL} {$seo.title} - {/if}{$info.web_title}</title>
<meta name="keywords" content="{if $seo.keyword neq NULL} {$seo.keyword} {else} {$info.web_keyword} {/if}" />
<meta name="description" content="{if $seo.description neq NULL} {$seo.description} {else} {$info.web_description} {/if}" />
<meta name="robots" content="noodp,index,follow" />
<meta name='revisit-after' content='1 days' />
<meta property="og:title" content="{if $seo.keyword neq NULL} {$seo.keyword} {else} {$info.web_keyword} {/if}" />
<meta property="og:type" content="{$domain}">
<meta property="og:url" content="{$this_link}" />
<meta property="og:image" content="{$domain}{$value.img}" />
<meta property="og:image:secure_url" content="{$domain}{$value.img}" >

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<meta name="apple-mobile-web-app-capable" content="yes">

 <link rel="stylesheet" type="text/css" href="webroot/css/view.css" />
<link rel='stylesheet' type="text/css" href="webroot/css/style.css">
<link rel='stylesheet' type="text/css" href="webroot/css/reset.css">
<link rel='stylesheet' type="text/css" href="webroot/css/category.css">
<link rel='stylesheet' type="text/css" href="webroot/css/wap.css">
<link rel='stylesheet' type="text/css" href="webroot/css/markets.css">
<link rel='stylesheet' type="text/css" href="webroot/css/catalogs.css">
<link rel='stylesheet' type="text/css" href="webroot/css/mobile.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase.js"></script>

<link rel="stylesheet" type="text/css" href="webroot/css/hoverZoomEtalage.css">

<script src="webroot/js/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" href="webroot/css/jquery-ui.css">
  <script src="webroot/js/jquery-ui.js"></script>
  <script src="webroot/js/jquery.mmenu.min.all.js"></script>
 
<!-- bxSlider CSS file -->
<script src="webroot/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<link href="webroot/js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript" src="webroot/js/hoverZoomjquery.etalage.min.js" ></script>

<script src="webroot/js/jspkg-archive/lib/jquery.hashchange.js" type="text/javascript"></script>
<script src="webroot/js/jspkg-archive/lib/jquery.easytabs.js" type="text/javascript"></script>
<script src="webroot/js/Show-More-master/example/example.js"></script>
<script src="webroot/js/jquery.validate.js"></script>
<script src="webroot/js/cart.js"></script>
<script src="webroot/js/main.js"></script>
