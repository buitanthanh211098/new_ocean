<style>
    .fix_use{
        padding-top: 20px!important;
    }
</style>
<div class="content about">

    <div class="use ">
                    <!-- <p>About 59809 results:<a href="">Mosaics</a>(54642), <a href="">Glass Processing Machinery</a>(2),<a href="">Building Glass</a>(380)</p> -->
                    <div class="backcrum mwap-x100">
                        <span>
                           {$category.site_map}
                        </span>

                    </div>
                    <div class="clear"></div>
                </div>
            </div> <!--end .about-->
            <div class="clear"></div>

            <div class="content ">
                <div class="use fix_use">
                    <div class="container-left mwap-x100">

                        <ul id="etalage">
                           <li style="background-image: none; display: list-item; opacity: 1;" class="etalage_thumb thumb_2  ">
					<img style="display: inline; width: 360px; height: 400px; opacity: 0.902931;" class="etalage_thumb_image" src="{$value.img}">
					<img class="etalage_source_image" src="{$value.img}">
				</li>
			{foreach from=$images key=k item=list}

			<li style="display: none; background-image: none; opacity: 0;" class="etalage_thumb thumb_1">

						<img style="display: inline; width: 360px; height: 400px; opacity: 1;" class="etalage_thumb_image" src="{$list}">
						<img class="etalage_source_image" src="{$list}" title="This is an optional description.">

				</li>

			{/foreach}

                        </ul>
                        <div class="product-info" style="width: 520px!important;">
                            <h3>{$value.name}</h3>
                            <div class="mar-top">{include file="../plugins/like.tpl"}</div>
                            <table>
                                <tbody>
                                    <tr>
                                        <th class="name">{if $value.promotions neq 0}Giá khuyến mãi:{else} Giá bán:{/if}  </th>
                                        <td>{$value.price_sale}</td>
                                    </tr>
                                    {if $value.promotions neq 0}
                                    <tr>
                                        <th>Giá cũ</th>
                                        <td style="text-decoration: line-through;">{$value.price_old}</td>
                                    </tr>
                                    {/if}
                                    <tr>
                                        <th>Mô tả: </th>
                                        <td>{$value.description}</td>
                                    </tr>
                                    <tr>
                                        <th>Port:</th>
                                        <td>CÔNG TY CỔ PHẦN XD VÀ TM New Ocean</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="button">
                                <div class="item">
                                    <a href="javascript:void(0)" class="contact add_cart" data-id="{$value.id}">
                                        <span class="ico-csb"></span>
                                        Đặt hàng
                                    </a>
                                </div>

                                <div class="item">
                                    <a href="contact" class="chatnow">
                                        <span class="ico"></span>
                                        Liên hệ!
                                    </a>
                                </div>
                            </div><!--end .button-->
<!--
                            <div class="functions">
                                <div class="item">
                                    <a href="" class="start-order">
                                        <span class="ico-ta"></span>
                                        Start Order
                                    </a>
                                </div>

                                <div class="item">
                                    <a href="" class="inquiry-cart">
                                        <span class="ico-ta"></span>
                                        Add to Inquiry Cart

                                    </a>
                                </div>

                                <div class="item">
                                    <a href="" class="favorites">
                                        <span class="ico-ta"></span>
                                        Add to My Favorites
                                    </a>
                                </div>
                            </div><!--end .functions-->

                            <div class="assurance mwap-x100">
                                <span class="background"></span>
                                <div class="desc">
                                    <div class="title">
                                        Bảo vệ người mua
                                       <!--  <span class="ta">ĐẠI SÀN.</span>
                                        <div class="sub-title">Follow the Trade Assurance process and get:</div>-->
                                    </div>
                                    <div class="list">
                                        <div class="line"><span class="point">•</span>Đảm bảo hoàn tiền nếu bạn không nhận được sản phẩm</div>
                                        <div class="line"><span class="point">•</span>Hoàn tiền hoặc tạm giữ sản phẩm không đúng mô tả</div>
                                        <div class="line"><span class="point">•</span>Khách hàng được đổi/ trả hàng lỗi, hỏng, không ưng ý trong vòng 7 ngày từ khi nhận hàng, hoàn toàn  <a id="J-xinbao-val" target="_blank"  href="" rel="nofollow" class="mark">miễn phí</a></div>
                                    </div>
                                    <a href="#" class="learn-more">Xem thêm›</a>
                                </div>

                            </div><!--end .assurance-->
                        </div><!--end .product-info-->

                        <div class="clear"></div>


                        <div id="tab-container" class="tab-container mwap-x100">
                            <ul class='etabs'>
                                <li class='tab'><a href="#tabs1-html">Thông tin chi tiết</a></li>
                                <li class='tab'><a href="#tabs1-js">Bình luận</a></li>
                                <li class='tab'><a href="#tabs1-css">Bảo hành dành cho khách hàng</a></li>
                            </ul>
                            <div id="tabs1-html" class="mwap-x100">
                                <div class="ls-company">
                                    <div class="box">
                                       {$value.content}

                                    </div>
<!--
                                    <div class="box basic-infomartion">
                                        <h3>BASIC INFOMARTION</h3>
                                        <table>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="box basic-infomartion">
                                        <h3>Trade & Market</h3>
                                        <table>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Main Markets:</td>
                                                <td class="info">
                                                    <ul>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                        <li>Domestic Market</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="box basic-infomartion">
                                        <h3>Factory Information</h3>
                                        <table>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Factory Size (Sq.meters):  </td>
                                                <td class="info">
                                                    10,000-30,000 square meters
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Factory Location:</td>
                                                <td class="info">No. 12, Xinlian Industrial Park, Lishui Town, Nanhai District, Foshan City, Guangdong Province, China  </td>
                                            </tr>
                                            <tr>
                                                <td class="icon"><span></span></td>
                                                <td class="name">Business Type:</td>
                                                <td class="info">Manufacturer, Trading Company &nbsp;</td>
                                            </tr>
                                        </table>
                                    </div> <!--end .basic-infomartion-->
<!--
                                    <div class="box basic-infomartion supplier">
                                        <h3>Supplier Assessment Report</h3>
                                        <a href="">Download and view Report</a>
                                        <p>Supplier Assessment Reports are detailed on-line reports about the supplier's capabilities. It helps you get all the information you need to trade confidently with suppliers.</p>
                                    </div> <!--end .basic-infomartion-->


                         <div class="content see-more">
                            <div class="title">
                                <h3>Sản phẩm<span class="mosaic">&nbsp;&nbsp;vừa xem</span>:</h3>



                            </div>
                             <div id="ProductViewList" class="box-items">
		                                 </div>


                        </div><!--end .see-more-->
                                </div><!--end .box-info-->
                            </div>
                            <div id="tabs1-js">
                                  <div class="fb-comments" data-href="{$this_link}" data-numposts="3"  data-width="760" data-colorscheme="light"></div>
                                <!-- content -->
                            </div>
                            <div id="tabs1-css">
                                <h2>CSS Styles for these tabs</h2>
                                <!-- content -->
                            </div>
                        </div>

                        <div class="detailts-product">

                        </div><!--end .detailts-product-->

                        <div class="clear"></div>
               <!--
                        <div class="contact">
                            <h3>Send your message to this supplier</h3>
                            <table>
                                <tr>
                                    <td class="first">To:</td>
                                    <td class="right">Tommy Zhang</td>
                                </tr>

                                <tr>
                                    <td class="first">To:</td>
                                    <td class="right text-send-mesage"><textarea></textarea>
                                        <p>Your message must be between 20-8000 characters</p>
                                        <div class="ui-balloon ui-balloon-lt">
                                            <div class="ui-balloon-content">
                                                <div class="fast-feedback-balloon-content">
                                                    <p>For better quotations, include:</p>
                                                    <p>- A self introduction</p>
                                                    <p>- Special requests, if any</p>
                                                    <a href="http://www.alibaba.com/help/inquiry_sample_2011.html" target="_blank"> View Sample</a>
                                                </div>

                                            </div>

                                            <a class="ui-balloon-arrow"></a>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="first">Quantity:</td>
                                    <td class="right">
                                        <input type="text" />
                                    <select>
                                        <option>avbca</option>
                                        <option>avbca</option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="first"></td>
                                    <td class="right">
                                        <input type="checkbox" style="float: left;">
                                        <span style="font-size: 12px;">Recommend matching suppliers if this supplier doesn’t contact me on Message Center within 24 hours</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="first"></td>
                                    <td class="right">
                                        <input type="checkbox" checked="" style="float: left;">
                                        <span style="font-size: 12px;"> I agree to share my Business Card to the supplier.</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="first"></td>
                                    <td class="right">
                                        <input type="submit" value="Send">
                                    </td>
                                </tr>

                            </table>
                        </div><!--end .contacnt-->
                        <!--  -->

                        <!--
                        <div class="read-more">
                            <div class="read-item blue">
                                <h3>Related Searches:</h3>
                                <div class="item">

                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                    </ul>
                                </div>

                                 <div class="item">
                                    <h3> </h3>
                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                    </ul>
                                </div>

                                 <div class="item">
                                    <h3> </h3>
                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="read-item red">
                                <h3>Related Searches:</h3>
                                <div class="item">

                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>

                                    </ul>
                                </div>

                                 <div class="item">
                                    <h3> </h3>
                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                    </ul>
                                </div>

                                 <div class="item">
                                    <h3> </h3>
                                    <ul>

                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                        <li><a href="">Curtain Searches</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div> -->

                    </div><!--end .container-left-->

                    <div class="container-right mwap-x100">
                       <h3>Hotline hỗ trợ</h3>
                    <div class="item10">
                        <ul>

						{foreach from=$support item=list}
						<li>
							<h4>{$list.name}</h4>
							<span class="phone">{$list.phone}</span>
						</li>
						{/foreach}

                        </ul>
                    </div>

                    </div><!--enb .container-right-->

                    <div class="clear"></div>
                </div>
            </div><!--end .contatiner-->