   {literal}
        <script>
            $(document).ready(function() {
                $('.bxsliders').bxSlider({
                    auto: true,
                    autoControls: true,
                    minSlides: 4,
                    maxSlides: 4

                });// bxslider

                $('ul.list-info li .des').hover(function() {
                    $(this).find('div.wap-detailts').slideDown(300);
                }, function() {
                    $(this).find('div.wap-detailts').slideUp(300);
                    $('ul.list-info li:last-child').find('.des div.wap-detailts').addClass('active');

                });

                $('ul.list-info li:last-child').find('.des div.wap-detailts').css('display', 'block');

            });

        </script>
        {/literal}
 
  <div class="content pad-top">
                <div class="use ">
                    <div class="category-left col-sub-cont">
                    
                         <h3>Danh mục sản phẩm</h3>
          <ul class="groups-list">
           {foreach from=$gcategory item=cate}
            <li class="groups-item"><i class="i-group-item"></i><a href="{$cate.link}">{$cate.name}</a>
              <ul class="show-group-item">
                {foreach from=$cate.child item=sub1}
              <li><i class="i-child-item"></i><a href="{$sub1.link}">{$sub1.name}</a></li>
              {/foreach}
              
              </ul>
             </li>
           {/foreach}
          </ul>
                    
                    <!--     <h3>Danh mục sản phẩm</h3>
                        <div class="clear"></div>
                        <ul>
                            <div class="showmore_one">
                            {foreach from = $cat_child item=list}
                                <li><a href="{$list.link}">{$list.name} </a><span>({$list.number})</span></li>
                                
                            {/foreach}
                            </div>
                        </ul>
                       -->
                     <h3>Sản phẩm xem nhiều</h3>
                  
                        <ul>
                        {foreach from = $max_view item = list}
                           {include file="../elements/product.tpl"}
                         {/foreach}
                           
                        </ul>
                  
                       
                    </div><!--end .category-left-->


                    <div class="box-container mwap-x100">
                      
                            <div class="backcrum mwap-x100">
                        <span>
                           {$category.site_map}({$number})
                        </span>
                        </div>

                        

                        <div class="content" style="padding-top: 20px;">
                        {foreach from = $result item = list}
                            <div class="item-product mwap-x100">
                                {include file="../elements/product.tpl"}

                                <div class="hide-box">
                                    <div class="img-tit">
                                        <a href="{$list.link}">
                                            <img src="{$list.img}">
                                        </a>
                                        <div class="favo">
                                            <span><a href="">{$list.view}(Lượt view)</a></span>
                                          
                                        </div>

                                    </div>
                                    <div class="desc">
                                       
                                        <a href="">{$list.name}</a>
                                        <p class="min-order"><span class="price">{$list.price_sale}</span></p>
                                         {if $list.promotions neq 0}<del>{$list.price_old}<span>/{$list.type}</span></del>{/if}

                                        <a href="" class="desc-a">{$info.contact}</a>

                                    </div>
                                <!-- 
                                    <div class="bot">
                                        <span class="icon"></span>
                                        <a href="#">Chat Now! </a>

                                        <a href="#" class="contact-list">Contact Details <i class="icon"></i></a>
                                    </div>
                                  -->
                                </div>

                            </div>
                        {/foreach}
                           

                        </div> 

                        <div class="pagging">
                                   {if $fields > $pagesize}<div class="paging">{$paging}</div>{/if}
                          
                        </div><!--end .pagging-->

                      

                        <div class="content see-more">
                            <div class="title">
                                <h3>Sản phẩm<span class="mosaic">&nbsp;&nbsp;vừa xem</span>:</h3>
                                
		                                
  
                            </div>
                             <div id="ProductViewList" class="box-items">
		                                 </div>
		                                 
                           
                        </div><!--end .see-more-->

                       




                </div><!--end .box-container-->

                <div class="content-right mwap-x100">
                    <h3>Sản phẩm nổi bật</h3>
                    
                        <ul>
                        {foreach from = $vip item = list}
                           {include file="../elements/product.tpl"}
                         {/foreach}
                           
                        </ul>
              

                </div><!--end .content-right-->



            </div><!--end .box-container-->
        </div><!--end .content-->