<h1 class="bor-btm">{$category}</h1>
<div class="mar-btm news">
	{if $result neq NULL}
		{foreach from=$result key=k item=list}
		<div class="bor-btm">
			<div class="wap-x25 mwap-x100">
				<div class="img mar-rgt bor">
					<a href="{$list.link}"><img src="{$list.img}"></a>
				</div>
			</div>
			<h3 class="mar-btm"><a href="{$list.link}" title="{$list.name}">{$list.name}</a></h3>
			<div class="">
				<p class=" align-jus">{$list.description}</p>
				<p class="view mar-top"><a href="{$list.link}">{$trans.gen_view}</a></p>
			</div>
			<div class="clear"></div>
		</div>
		{/foreach}
		{if $fields > $pagesize}<div class="paging">{$paging}</div>{/if}
		
	{else}
	<h3 class="name"><span class="cont">{$trans.news_none}</span></h3>
	{/if}	
</div>
