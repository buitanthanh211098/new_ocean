<div id="Register">
<div class="use">

	<div class="wap-x55">
		<div class="pad mar-3rgt">
		</div>
	</div>
	
	<div class="wap-x45">
		<div class="bor pad-big mar-3mid bg">
			<h1 class="bor-btm">Đăng nhập</h1>
			<div class="mar-2mid">
				<p class="mar-mid">{$message}</p>
				<form method="post" action="" class="mar-xmid" id="FrmValidate">
				<table class="table">
					<tr>
						<td width="140">Tên đăng nhập</td>
						<td><input type="text" name="email" class="required email fnt-x200"></td>
					</tr>
					<tr>
						<td>Mật khẩu</td>
						<td><input type="password" name="password" class="required fnt-x200"></td>
					</tr>
					<tr>
						<td></td>
						<td><a href="?mod=member&site=passreset">Bạn quên mật khẩu ?</a></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="checkbox" name="remember"> Ghi nhớ đăng nhập</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" name="FrmLogin" value="Đăng Nhập"></td>
					</tr>
				</table>
				</form>
				<p class="mar-2top">Nếu chưa có tài khoản, vui lòng <a href="dang-ky.html">đăng ký</a> tại đây !</p>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
</div>
</div>