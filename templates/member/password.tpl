<div class="pad">
	<h1 class="bor-btm">Thay đổi mật khẩu</h1>
	<p class="mar-mid">{$message}</p>
	<form method="post" action="" class="mar-xmid" id="Validate">
	<table class="table">
		<tr>
			<td width="180">nhập mật khẩu cũ (*)</td>
			<td><input type="password" name="old" class="fnt-x200 required" minlength="6"></td>
		</tr>
		<tr>
			<td>Mật khẩu mới (*)</td>
			<td><input type="password" name="new" class="fnt-x200 required" minlength="6"></td>
		</tr>
		<tr>
			<td>Xác nhận mật khẩu mới (*)</td>
			<td><input type="password" name="re" class="fnt-x200 required" minlength="6"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="FrmSubmit" value="Thay đổi mật khẩu" class=""></td>
		</tr>
	</table>
	</form>
</div>
