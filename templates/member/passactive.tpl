<div class="pad-big">

	<div class="wap-x55">
		<div class="bor pad mar-3rgt">
		
			
			
		</div>
	</div>
	
	<div class="wap-x45">
		<div class="bor pad">
			<h1 class="bor-btm">Quên mật khẩu</h1>
			<div class="mar-2mid">
				<p class="mar-mid bold">{$message}</p>
				<form method="post" action="" class="mar-xmid" id="FrmValidate">
				<table class="table">
					<tr>
						<td width="140" class="bold">Nhập mật khẩu mới: </td>
						<td><input type="password" name="pass" class="required fnt-x200"></td>
					</tr>
					<tr>
						<td width="140" class="bold">Nhập lại mật khẩu: </td>
						<td><input type="password" name="repass" class="required fnt-x200"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" name="FrmReset" value="Lưu mật khẩu"></td>
					</tr>
				</table>
				</form>
				<p class="mar-2top">Nếu chưa có tài khoản, vui lòng <a href="register.html">đăng ký</a> tại đây !</p>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
</div>