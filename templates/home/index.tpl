
  <div class="content Most-Popular-Products">

      <div class="use">

   <div class="wap-x70 mwap-x100">
   <div class="wap-x100 mwap-x100">
    <h1 class="bor-btm ">Sản phẩm nổi bật</h1>
    <div class="mar-btm mwap-x100 ">
        <div class="module-hot" style="width: 100%!important;border: none">
            {foreach from=$vip item=list}
{*                <div class="hot-item" style="width: 33%!important;">*}
{*                    <div class="img-item">*}
{*                        <a href="{$list.link}"> <img src="{$list.img}"></a>*}
{*                    </div>*}
{*                    <div class="desc">*}
{*                        <a href="{$list.link}">*}
{*                            <p><a href="{$list.link}">{$list.name}</a></p>*}
{*                            <span>{$list.view} Lượt xem<br />*}
{*                                        <strong>{$list.price_sale}</strong>*}

{*                                    </span>*}
{*                        </a>*}
{*                    </div>*}
{*                </div>*}
                <div class="item-product mwap-x100">
                    {include file="../elements/product.tpl"}
                    <div class="hide-box">
                        <div class="img-tit">
                            <a href="{$list.link}">
                                <img src="{$list.img}">
                            </a>
                            <div class="favo">
                                <span><a href="">{$list.view}(Lượt view)</a></span>

                            </div>

                        </div>
                        <div class="desc">

                            <a href="">{$list.name}</a>
                            <p class="min-order"><span class="price">{$list.price_sale}</span></p>
                            {if $list.promotions neq 0}<del>{$list.price_old}<span>/{$list.type}</span></del>{/if}

                            <a href="" class="desc-a">{$info.contact}</a>

                        </div>
                        <!--
                            <div class="bot">
                                <span class="icon"></span>
                                <a href="#">Chat Now! </a>

                                <a href="#" class="contact-list">Contact Details <i class="icon"></i></a>
                            </div>
                          -->
                    </div>

                </div>

            {/foreach}

        </div>

    </div>

</div> <!--End class wap100-->
</div> <!--End class wap 70-->

<div class="wap-x25 mar-3lft mwap-x100">
    <h1 class="bor-btm ">Tin tức mới</h1>
    {if $news neq NULL}
        {foreach from=$news key=k item=list}
            {if $k > 2}{continue}{/if}
            <div class="mar-btm">
                <div class="wap-x45 mwap-x15" >
                    <div class="img mar-rgt bor">
                        <a href="{$list.link}"><img src="{$list.img}" height="120"></a>
                    </div>
                </div>
                <h3 class="mar-btm"><a href="{$list.link}" title="{$list.name}">{$list.name}</a></h3>
                <p><i>{$list.created}</i></p>
{*                <div class="">*}
{*                    <p class=" align-jus">{$list.description}</p>*}
{*                </div>*}
                <div class="clear"></div>
            </div>
        {/foreach}
        {if $fields > $pagesize}<div class="paging">{$paging}</div>{/if}

    {else}
        <h3 class="name"><span class="cont">{$trans.news_none}</span></h3>
    {/if}

    <h1 class="bor-btm ">Video giới thiệu</h1>
    
    <div class="mar-btm">
		{foreach from=$video key=k item=list}
            {if $k > 1}{continue}{/if}
            <iframe width="100%" height="195" src="http://www.youtube.com/embed/{$list.code}?autoplay=0" frameborder="0" allowfullscreen></iframe>
		{/foreach}
	</div>
	 
	<!-- include file chung khoán -->
{*	<h1 class="bor-btm ">Chứng khoán</h1>*}

<script>
function switchActive(i){
		if(i == 0){
			document.getElementById('cHoSTC').className = 'cActive';
			document.getElementById('cHaSTC').className = 'cNoActive';
			document.getElementById('aHoSTC').className = 'Active';
			document.getElementById('aHaSTC').className = 'NoActive';
		}
		else{
			document.getElementById('cHoSTC').className = 'cNoActive';
			document.getElementById('cHaSTC').className = 'cActive';
			document.getElementById('aHoSTC').className = 'NoActive';
			document.getElementById('aHaSTC').className = 'Active';
		}
	}

</script>

{*<TABLE cellSpacing=0 cellPadding=0 border=0 width="300">*}
{*              <TBODY>*}
{*              <TR>*}
{*                <TD vAlign=top width="100%">*}
{*                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>*}
{*                    <TBODY>*}
{*                    <TR>*}
{*                       <TD class=cActive id=cHoSTC align="center" valign="middle" width=58 *}
{*                      height=20 alt=""><A class=Active id=aHoSTC *}
{*                        onclick=switchActive(0); *}
{*                        href="http://www.vnexpress.net/User/ck/hcms/HCMStockSmall.asp" *}
{*                        target=ifrmContent>HoSTC</A></TD>*}
{*                         <TD class=cNoActive id=cHaSTC align="center" valign="middle" width=58 *}
{*                      height=20 alt=""><A class=NoActive id=aHaSTC *}
{*                        onclick=switchActive(1); *}
{*                        href="http://www.vnexpress.net/User/ck/hns/HNStockSmall.asp" *}
{*                        target=ifrmContent>HaSTC</A></TD>*}
{*                      </TR></TBODY></TABLE>*}
{*                  <TABLE class=tbHeader cellSpacing=1 cellPadding=1 width="100%"*}
{*                  border=0>*}
{*                    <TBODY>*}
{*                    <TR align=middle>*}
{*                      <TD class=cHeader align=middle width=25 rowSpan=2>Mã CK</TD>*}
{*                      <TD class=cHeader align=middle width=28 rowSpan=2>TC</TD>*}
{*                      <TD class=cHeader noWrap align=middle *}
{*                        colSpan=2>Khớp lệnh</TD>*}
{*                      <TD class=cHeader align=middle width=36 *}
{*                    rowSpan=2>+/-</TD>*}
{*                    </TR>*}
{*                    <TR>*}
{*                      <TD class=cHeaderN align=middle width=30>Giá</TD>*}
{*                  	  <TD class=cHeaderN align=middle width=30>KL</TD>*}
{*                    </TR></TBODY></TABLE><IFRAME id=ifrmContent *}
{*                  border=false name=ifrmContent *}
{*                  src="http://www.vnexpress.net/User/ck/hns/HNStockSmall.asp" *}
{*                  frameBorder=0 noResize width="100%" scrolling=no *}
{*                  height=200></IFRAME>*}

{*                </TD></TR></TBODY></TABLE>*}
            </TD></TR></TBODY></TABLE>

	<!-- end include file chungkhoan -->
    
</div>

            <div class="content browse-top-category">
                <div class="use">
                    <h1>Danh mục nổi bật</h1>

                   
                    <div class="slide">
                        <ul class="bxsliders most-box">
                    {foreach from = $gcategory_home item=list}
                   
                        <li class="mwap-x70">
                            <div class="img mwap-x45">
                                <a href="{$list.link}"><img src="{$list.image}"  width="285" height="285"></a>
                                <div class="desc">{$list.name}</div>
                            </div>


                        </li>
                         
                      {/foreach}
                      

                    </ul>
                    </div>
                </div>

   <div class="partner mwap-x100">
 <div class="use"> 
  <h1><a href="javascript:void(0)">Khách hàng Đối tác</a></h1>
 <ul class="bx-partner">
 {foreach from=$gallery_p5 item = list}
  <li><a href="{$list.link}" target="_blank"><img src="{$list.img}" width="200" height="100" /></a></li>
 {/foreach}
 
</ul>
</div>
<div class="clear"></div>
</div>
 
</ul>

                </div><!--End class content browse-top-category-->
                
            

            <div class="content laceclothing">
                <div class="use">
                   <h1></h1>

                <div class="wap-x65 mwap-x100">
			{foreach from=$menu_p4 item=list}
			<div class="wap-x25 mwap-x100">
				<div class="pad">
					<h3>{$list.name}</h3>
					{foreach from=$list.child item=sub1}
					<p><a href="{$sub1.alias}">{$sub1.name}</a></p>
					{/foreach}
				</div>
                
                 
                
			</div>
			{/foreach}     
            
                <div class="wap-x25 mwap-x100">
              <div class="pad ft-social">
            <h3>Kết nối với chúng tôi</h3>
            <p><a href="https://www.facebook.com/DAISANINAX?fref=ts"><span class="fb"></span> Facebook</a></p>
            <p><a href="#"><span class="gg"></span>Google+</a></p>
            <p><a href="#"><span class="yy"></span>Youtube</a></p>
            <p><a href="#"><span class="tw"></span>Twitter</a></p>
            <p><a href="#"><span class="pt"></span>Pinterest</a></p>
            </div>
            </div>    
            
{*         <div class="wap-x25 mwap-x100">*}
{*              <div class="pad">*}
{*            <h3>Được chứng nhận</h3>*}
{*         <p><a href="http://www.online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=13108"><img src="{$host}webroot/css/images/chung-nhan.png" /></a></p>*}
{*            *}
{*            </div>*}
{*            </div>     *}
{*            *}
                         
			<div class="clear"></div>
			
         
</div><!--End class wap x65-->
		
 		
		<div class="wap-x35 mwap-x100">
			<div class="pad">
				<h3>Sản phẩm có bán tại các địa chỉ</h3>
				<ul style="list-style:square;">
					{foreach from=$gpage key=k item=list}
					{if $k<=4}
					<li><a href="{$list.link}">{$list.address} - {$list.name} - hotline: {$list.hotline}</a></li>
					{/if}
					{/foreach}
				</ul>
			</div>
		</div>
		
		<div class="clear"></div>

                    
            </div><!--end .laceclothing-->
            
             </div><!--end .use-->