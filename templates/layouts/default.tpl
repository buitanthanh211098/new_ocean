<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        {include file="../elements/meta.tpl"}
      
      
        <script>
            $(document).ready(function() {
                $('.bxslider').bxSlider({
                    auto: true,
                    autoControls: true
                });// bxslider

                $('#etalage').etalage({
                    thumb_image_width: 352,
                    thumb_image_height: 352,
                    source_image_width: 900,
                    source_image_height: 1200,
                    show_hint: true,
                    click_callback: function(image_anchor, instance_id) {
                        alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                    }
                });

                $('#tab-container').easytabs();

            });
        </script>
        
        

    </head>
    <body>
        <div id="wrapper">
        <!-- header -->
              {include file="../elements/header.tpl"}
              <!--end .header-->
            <div class="clear"></div>

             {literal}
        <script>
            $(document).ready(function() {
                $('.bxsliders').bxSlider({
                    auto: true,
                    autoControls: true,
                    minSlides: 4,
                    maxSlides: 4

                });// bxslider

                $('ul.list-info li .des').hover(function() {
                    $(this).find('div.wap-detailts').slideDown(300);
                }, function() {
                    $(this).find('div.wap-detailts').slideUp(300);
                    $('ul.list-info li:last-child').find('.des div.wap-detailts').addClass('active');

                });

                $('ul.list-info li:last-child').find('.des div.wap-detailts').css('display', 'block');

            });

        </script>
        {/literal}
 
  <div class="content pad-top">
                <div class="use ">
                 <div class="category-left">
                    {include file="../elements/sidebar.tpl"}
                    </div>
                    <!--end .category-left-->


                    <div class="box-container mwap-x100">
                      
                         {include file=$content}
                         <!--end .box-container-->




                </div><!--end .box-container-->

                <div class="content-right mwap-x100">
                  
                
                     <div class="clear"></div>
                    <h3>Hotline hỗ trợ</h3>
                    <div class="item10">
                        <ul>
                       
						{foreach from=$support item=list}
						<li>
							<h4>{$list.name}</h4>
							<span class="phone">{$list.phone}</span>
						</li>
						{/foreach}
       
                        </ul>
                    </div>
                    
                    <img src="{$host}webroot/images/Capture.PNG" width="200" height="200">

                </div><!--end .content-right-->



            </div><!--end .box-container-->
        </div><!--end .content-->
            
            
          <div id="footer mwap-x100">    
               {include file="../elements/footer.tpl"}
            </div><!--end #footer-->
            
    </body>
</html>