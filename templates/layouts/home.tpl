<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    {include file="../elements/meta.tpl"}

</head>
<style>
    div.box-category .slide {
        width: 80%!important;
        float: left;
    }


</style>
<body>
<div id="wrapper">
    <!-- header -->
    {include file="../elements/header.tpl"}
    <!--end .header-->

    <div class="content category box-category">
        <div class="use">
            {include file="../elements/category.tpl"}
            {literal}
                <script>
                    $(document).ready(function () {
                        $('.bxslider').bxSlider({
                            auto: true,
                            autoControls: true

                        });
                    }); // bxslider


                    $(document).ready(function () {
                        $('.bxsliders').bxSlider({
                            auto: true,
                            autoControls: true,
                            slideWidth: 285,
                            minSlides: 5,
                            maxSlides: 5,
                            slideMargin: 20,
                            moveSlides: 1,

                        });
                    }); // bxslider

                    $(document).ready(function () {
                        $('.bx-partner').bxSlider({
                            auto: true,
                            autoControls: false,
                            slideWidth: 285,
                            minSlides: 5,
                            maxSlides: 5,
                            slideMargin: 20,
                            moveSlides: 1,
                            controls: true,
                            speed: 1000,
                            pager: false

                        });
                    }); // bxslider


                </script>
            {/literal}

            <div id="demo" class="carousel slide"  data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    {foreach from=$gallery_p2 key=k item=list}
                        <li data-target="#demo" data-slide-to="{$k}" {if $k == 0}class="active"{/if}></li>

                    {/foreach}

                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    {foreach from=$gallery_p2 key=k item=list}
                        <div class="carousel-item {if $k == 0}active{/if}">
                            <a href="{$list.link}" target="_blank">
                                <img src="{$list.img}" alt="{$list.alt}" width="100%" height="428px">
                            </a>
                        </div>
                    {/foreach}
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>

            {*            <div class="slide">*}
            {*                <div class="slider">*}
            {*                    <ul class="bxslider">*}
            {*                        {foreach from=$gallery_p2 key=k item=list}*}
            {*                            <li><img src="{$list.img}" title="" alt="{$list.alt}"/></li>*}
            {*                        {/foreach}*}
            {*                    </ul>*}
            {*                </div>*}


            {*            </div>*}
        </div>
    </div><!--end .menu-->


    <!-- content -->
    {include file = $content}
    <!-- end content -->

</div>
</div>

<div id="footer">

    {include file="../elements/footer.tpl"}
</div><!--end #footer-->

</body>
</html>
