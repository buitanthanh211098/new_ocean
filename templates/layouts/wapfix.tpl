<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        {include file="../elements/meta.tpl"}
      
      
        <script>
            $(document).ready(function() {
                $('.bxslider').bxSlider({
                    auto: true,
                    autoControls: true
                });// bxslider

                $('#etalage').etalage({
                    thumb_image_width: 352,
                    thumb_image_height: 352,
                    source_image_width: 900,
                    source_image_height: 1200,
                    show_hint: true,
                    click_callback: function(image_anchor, instance_id) {
                        alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                    }
                });

                $('#tab-container').easytabs();

            });
        </script>
        
        

    </head>
    <body>
        <div id="wrapper">
        <!-- header -->
              {include file="../elements/header.tpl"}
              <!--end .header-->
            <div class="clear"></div>

           {include file= $content}
            
            
            <div id="footer">
                <hr>

             {include file="../elements/footer.tpl"}
            </div><!--end #footer-->
            
    </body>
</html>