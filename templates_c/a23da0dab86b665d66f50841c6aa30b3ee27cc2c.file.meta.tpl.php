<?php /* Smarty version Smarty-3.1.18, created on 2019-08-16 03:36:50
         compiled from "C:\xampp\htdocs\new_ocean\templates\elements\meta.tpl" */ ?>
<?php /*%%SmartyHeaderCode:42955d5608b2a79f06-35151548%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a23da0dab86b665d66f50841c6aa30b3ee27cc2c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\new_ocean\\templates\\elements\\meta.tpl',
      1 => 1564642077,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '42955d5608b2a79f06-35151548',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'domain' => 0,
    'seo' => 0,
    'info' => 0,
    'this_link' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5d5608b2b5c235_62866109',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d5608b2b5c235_62866109')) {function content_5d5608b2b5c235_62866109($_smarty_tpl) {?><meta charset="utf-8">
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
<meta http-equiv="content-language" content="vi" />
<base href="<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
"></base>
<title><?php if ($_smarty_tpl->tpl_vars['seo']->value['title']!=null) {?> <?php echo $_smarty_tpl->tpl_vars['seo']->value['title'];?>
 - <?php }?><?php echo $_smarty_tpl->tpl_vars['info']->value['web_title'];?>
</title>
<meta name="keywords" content="<?php if ($_smarty_tpl->tpl_vars['seo']->value['keyword']!=null) {?> <?php echo $_smarty_tpl->tpl_vars['seo']->value['keyword'];?>
 <?php } else { ?> <?php echo $_smarty_tpl->tpl_vars['info']->value['web_keyword'];?>
 <?php }?>" />
<meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['seo']->value['description']!=null) {?> <?php echo $_smarty_tpl->tpl_vars['seo']->value['description'];?>
 <?php } else { ?> <?php echo $_smarty_tpl->tpl_vars['info']->value['web_description'];?>
 <?php }?>" />
<meta name="robots" content="noodp,index,follow" />
<meta name='revisit-after' content='1 days' />
<meta property="og:title" content="<?php if ($_smarty_tpl->tpl_vars['seo']->value['keyword']!=null) {?> <?php echo $_smarty_tpl->tpl_vars['seo']->value['keyword'];?>
 <?php } else { ?> <?php echo $_smarty_tpl->tpl_vars['info']->value['web_keyword'];?>
 <?php }?>" />
<meta property="og:type" content="<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
">
<meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['this_link']->value;?>
" />
<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
" />
<meta property="og:image:secure_url" content="<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
" >

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<meta name="apple-mobile-web-app-capable" content="yes">

 <link rel="stylesheet" type="text/css" href="webroot/css/view.css" />
<link rel='stylesheet' type="text/css" href="webroot/css/style.css">
<link rel='stylesheet' type="text/css" href="webroot/css/reset.css">
<link rel='stylesheet' type="text/css" href="webroot/css/category.css">
<link rel='stylesheet' type="text/css" href="webroot/css/wap.css">
<link rel='stylesheet' type="text/css" href="webroot/css/markets.css">
<link rel='stylesheet' type="text/css" href="webroot/css/catalogs.css">
<link rel='stylesheet' type="text/css" href="webroot/css/mobile.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase.js"></script>

<link rel="stylesheet" type="text/css" href="webroot/css/hoverZoomEtalage.css">

<script src="webroot/js/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" href="webroot/css/jquery-ui.css">
  <script src="webroot/js/jquery-ui.js"></script>
  <script src="webroot/js/jquery.mmenu.min.all.js"></script>
 
<!-- bxSlider CSS file -->
<script src="webroot/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<link href="webroot/js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript" src="webroot/js/hoverZoomjquery.etalage.min.js" ></script>

<script src="webroot/js/jspkg-archive/lib/jquery.hashchange.js" type="text/javascript"></script>
<script src="webroot/js/jspkg-archive/lib/jquery.easytabs.js" type="text/javascript"></script>
<script src="webroot/js/Show-More-master/example/example.js"></script>
<script src="webroot/js/jquery.validate.js"></script>
<script src="webroot/js/cart.js"></script>
<script src="webroot/js/main.js"></script>
<?php }} ?>
