<?php /* Smarty version Smarty-3.1.18, created on 2019-08-16 03:53:25
         compiled from "templates\layouts\wapfix.tpl" */ ?>
<?php /*%%SmartyHeaderCode:153265d560c95cb8688-10414310%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bf8b64a543e7f927a7435c0b23fd29a86bcf5ff3' => 
    array (
      0 => 'templates\\layouts\\wapfix.tpl',
      1 => 1563778316,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '153265d560c95cb8688-10414310',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5d560c95d25932_42550452',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d560c95d25932_42550452')) {function content_5d560c95d25932_42550452($_smarty_tpl) {?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php echo $_smarty_tpl->getSubTemplate ("../elements/meta.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      
      
        <script>
            $(document).ready(function() {
                $('.bxslider').bxSlider({
                    auto: true,
                    autoControls: true
                });// bxslider

                $('#etalage').etalage({
                    thumb_image_width: 352,
                    thumb_image_height: 352,
                    source_image_width: 900,
                    source_image_height: 1200,
                    show_hint: true,
                    click_callback: function(image_anchor, instance_id) {
                        alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                    }
                });

                $('#tab-container').easytabs();

            });
        </script>
        
        

    </head>
    <body>
        <div id="wrapper">
        <!-- header -->
              <?php echo $_smarty_tpl->getSubTemplate ("../elements/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

              <!--end .header-->
            <div class="clear"></div>

           <?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['content']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            
            
            <div id="footer">
                <hr>

             <?php echo $_smarty_tpl->getSubTemplate ("../elements/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div><!--end #footer-->
            
    </body>
</html><?php }} ?>
