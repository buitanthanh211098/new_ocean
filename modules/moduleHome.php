<?php

class Home extends Generals{
	
	function index(){
		$this->general();
		global $lang, $smarty;

		
		$search = isset($_SESSION['search_posts']) ? $_SESSION['search_posts'] : array("province"=>0, "district"=>0, "ward"=>0, "street"=>0);
		$local_active = $search['province'];
	
		$result = array();
		$sql = "SELECT a.id,lang.name,a.image FROM product_category AS a
				LEFT JOIN product_category_lang AS lang ON a.id=lang.field_id
				WHERE a.active=1 AND lang.lang=$lang AND a.parent_id=0";

		$query = $this->query($sql);
		while($item = $this->fetch_array($query)){
			$id1 = $this->get_arr_id("product_category", $item['id']);
			
			
			$sql1 = "SELECT a.id,a.name,a.view,a.page_id,a.category_id, a.price,a.promotions,a.image, a.type, page.name AS page FROM product AS a
					LEFT JOIN tbl_page AS page ON a.page_id=page.id
					WHERE a.active=1 AND a.category_id IN ($id1) 
					ORDER BY a.id
					";
			
			$item['child'] = $this->get_list("product_category", $item['id'], ALIAS_PRODUCT, 0, 6);
			$item['product'] = $this->get_products($sql1, 20);
			
			
			
	
			//var_dump($item['product']);
		    $item['link'] = ALIAS_PRODUCT.$item['id']."/".$this->str_convert($item['name']) . ".html";
			$item['alt'] = $this->str_convert($item['name']) . "jpg";
			$item['img'] = $this->get_image(PRODUCT_UPLOAD . "categories/", $item['image']);
			$result[] = $item;
		}
		$smarty->assign('result', $result);
		
		# Lấy sản phẩm vip
		$vip_sql = "SELECT id,name,image,category_id,price,promotions,page_id,view,type FROM product
		WHERE active=1 AND future=1
		ORDER BY order_by ASC
		";
		$vip = $this->get_products($vip_sql, 12);
		$smarty->assign('vip', $vip);
		
		# Lấy sản phẩm xem nhiều
		$view_sql = "SELECT id,name,image,category_id,price,promotions,page_id,view,type FROM product
		WHERE active=1 
		ORDER BY view DESC
		";
		$max_view = $this->get_products($view_sql, 8);
		$smarty->assign('max_view', $max_view);
		
		/* bÀI VIẾT MỚI */
		$sql_article = "SELECT a.*,lg.name,lg.description FROM article AS a
		LEFT JOIN article_lang AS lg ON a.id=lg.field_id AND lg.lang=$lang
		WHERE a.active=1 ";
		//$sql_article .= " AND a.category_id = " . $value['category_id'];
		$sql_article .= " ORDER BY a.future ASC, a.id DESC LIMIT 0,8";
		$article = $this->get_articles($sql_article);
		$smarty->assign("news", $article);
		
		
		$shop_vip = $this->find("SELECT * FROM tbl_page WHERE active=1");
		$smarty->assign("shop_vip", $shop_vip);
		
		
	
		$smarty->display("home.tpl");
	}
	
	
	function images(){
		$this->general();
		
		$images = $this->gen_gallery_by_position(5, 30);
		$smarty->assign('images', $images);
		
		$videos_default = $this->gen_video(NULL,1);
		$smarty->assign('videos_default', $videos_default);
		
		$videos = $this->gen_video();
		$smarty->assign('videos', $videos);
		
		$smarty->display("wapfix.tpl");
	}
	

	function add_email(){
		$this->general();
		$message = "Vui lòng nhập email !";
		
		
		if(isset($_POST['SubmitEmail'])){
			$email = $_POST['email'];
			$check = filter_var($email, FILTER_VALIDATE_EMAIL);
			//var_dump($check);
			if($check){
				$exits = $this->check_exist("SELECT id FROM tbl_email WHERE email='$email'");
				if($exits){
					$message = "Email đã đăng ký !";
				}
				else{
					$data['email'] = $email;
					$this->query_insert("tbl_email", $data);
					$message = "Đăng ký email thành công !";
				}
			}
		}
		$smarty->assign('message', $message);
		$smarty->display(DEFAULT_LAYOUT);
	}
	
	
	function question(){
		$this->general();

		$result = array();
		
		
		$sql = "SELECT * FROM question WHERE active=1 AND title IS NOT NULL AND answer IS NOT NULL ORDER BY order_by ASC, id DESC";
		$sql = $this->pagination($sql, 4);
		$query = $this->query($sql);
		while ($item = $this->fetch_array($query)){
			$result[] = $item;
		}
		$smarty->assign('result', $result);
		
		$smarty->assign('gallery_p3', $this->gen_gallery_by_position(3, 12));//Quang cao

		
		if(isset($_POST['FrmSubmit'])){
			$data['title'] = $_POST['name'];
			$data['answer'] = $_POST['content'];
			if($data['title']=="" || $data['title']==NULL){
				$this->alert("Vui lòng nhập nội dung câu hỏi !");
				$this->redirect_back();
			}
			else{
				$data['created'] = $this->my_time()->time_now();
				$this->query_insert("question", $data);
				$this->alert("Cảm ơn bạn đã gửi ý kiến cho chúng tôi !");
				$this->redirect_script(DOMAIN);
			}
		}
		
		
		$smarty->display("wapfix.tpl");
	}

	
	function question_add(){
		$this->general();
		
		if(isset($_POST['FrmSubmit'])){
			$data['title'] = $_POST['title'];
			if($data['title']=="" || $data['title']==NULL){
				$this->alert("Vui lòng nhập nội dung câu hỏi !");
				$this->redirect_back();
			}
			else{
				$data['created'] = $this->my_time()->time_now();
				$this->query_insert("question", $data);
				$this->alert("Cảm ơn bạn đã gửi câu hỏi !");
				$this->redirect_script(DOMAIN);
			}
		}
		
		$smarty->display(DEFAULT_LAYOUT);
	}
	
	function contact(){
		$this->general();
		global $lang, $smarty;
		if(isset($_POST["FrmSubmit"])){
			
			$captcha = $_SESSION["vshops_capcha"];
			if($captcha != $_POST["captcha"]){
				$this->alert("Mã kiểm tra không đúng !");
				$this->redirect_back();
			}
			else {
				$data["name"] = $_POST["name"];
				$data["email"] = $_POST["email"];
				$data["phone"] = $_POST["phone"];
				$data["content"] = $_POST["content"];
				$data["created"] = $this->time_now();
				if($data["name"] != "" && $data["email"] != "" && $data["content"] != ""){
					if($cennect_id = $this->query_insert("contact", $data)){
						
						$mail_conf = $this->find_one("SELECT * FROM conf_mail WHERE active=1 LIMIT 1");
						
						$title = "Liên Hệ Mã: ".$cennect_id;
						$content = "<div style='border:1px solid #ccc; background: #f2f2f2; padding: 10px; margin-top:10px;'>";
						$content .= "<h3>Thông tin liên hệ</h3>";
						$content .= "<p> Tên: ".$data["name"]."</p>";
						$content .= "<p>	Nội dung: ".$data["content"]."</p>";
						$content .= "<p>Email: ".$data["email"]."</p>";
						$content .= "<a href='tel:".$data["phone"]."'>Điện thoại: ".$data["phone"]."</a>";
						$content .= "</div>";
						$mail = $this->send_mail($mail_conf, $title, $content);	
						$this->alert("Thông tin của bạn sẽ được phản hồi sớm. Cảm ơn!");
						$this->redirect_script(DOMAIN);
					}
					else{
						$this->alert("error");
					}
				}
				else{
					$this->alert("Please enter data !");
					$this->redirect_back();
				}
			}
		}
		
		$smarty->display("map.tpl");
	}
	
	
}