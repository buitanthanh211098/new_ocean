<div class="sortable row-fluid">
	<a data-rel="tooltip" title="6 new members." class="well span3 top-block" href="?mod=member&site=manager">
		<span class="icon32 icon-red icon-user"></span>
		<div>Total Members</div>
		<div>0</div>
		<span class="notification">0</span>
	</a>

	<a data-rel="tooltip" title="4 new pro members." class="well span3 top-block" href="#">
		<span class="icon32 icon-color icon-star-on"></span>
		<div>Pro Members</div>
		<div>228</div>
		<span class="notification green">4</span>
	</a>

	<a data-rel="tooltip" title="{$number.invoice_new} hoá đơn mới" class="well span3 top-block" href="#">
		<span class="icon32 icon-color icon-cart"></span>
		<div>Hoá đơn</div>
		<div>{$number.invoice}</div>
		<span class="notification yellow">{$number.invoice_new}</span>
	</a>
	
	<a data-rel="tooltip" title="{$number.contact_new} liên hệ mới" class="well span3 top-block" href="#">
		<span class="icon32 icon-color icon-envelope-closed"></span>
		<div>Liên hệ</div>
		<div>{$number.contact}</div>
		<span class="notification red">{$number.contact_new}</span>
	</a>
</div>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well">
			<h2><i class="icon-info-sign"></i> Introduction</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<h1>Charisma <small>free, premium quality, responsive, multiple skin admin template.</small></h1>
			<p>Its a live demo of the template. I have created Charisma to ease the repeat work I have to do on my projects. Now I re-use Charisma as a base for my admin panel work and I am sharing it with you :)</p>
			<p><b>All pages in the user are functional, take a look at all, please share this with your followers.</b></p>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
