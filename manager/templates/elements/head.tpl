<base href="{$admin}"></base>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="DaiSan Templates pro">
<meta name="author" content="Muhammad Usman">

<!-- The styles -->
<link href="webroots/css/bootstrap-cerulean.css" rel="stylesheet">
<link href="webroots/css/style.css" rel="stylesheet">
<link href="webroots/css/bootstrap-responsive.css" rel="stylesheet">
<link href="webroots/css/charisma-app.css" rel="stylesheet">
<link href="webroots/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href='webroots/css/fullcalendar.css' rel='stylesheet'>
<link href='webroots/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
<link href='webroots/css/chosen.css' rel='stylesheet'>
<link href='webroots/css/uniform.default.css' rel='stylesheet'>
<link href='webroots/css/colorbox.css' rel='stylesheet'>
<link href='webroots/css/jquery.cleditor.css' rel='stylesheet'>
<link href='webroots/css/jquery.noty.css' rel='stylesheet'>
<link href='webroots/css/noty_theme_default.css' rel='stylesheet'>
<link href='webroots/css/elfinder.min.css' rel='stylesheet'>
<link href='webroots/css/elfinder.theme.css' rel='stylesheet'>
<link href='webroots/css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='webroots/css/opa-icons.css' rel='stylesheet'>
<link href='webroots/css/uploadify.css' rel='stylesheet'>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="webroots/img/favicon.ico">
