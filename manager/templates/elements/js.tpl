<!-- external javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- jQuery -->
<script src="webroots/js/jquery-1.7.2.min.js"></script>
<!-- jQuery UI -->
<script src="webroots/js/jquery-ui-1.8.21.custom.min.js"></script>
<!-- transition / effect library -->
<script src="webroots/js/bootstrap-transition.js"></script>
<!-- alert enhancer library -->
<script src="webroots/js/bootstrap-alert.js"></script>
<!-- modal / dialog library -->
<script src="webroots/js/bootstrap-modal.js"></script>
<!-- custom dropdown library -->
<script src="webroots/js/bootstrap-dropdown.js"></script>
<!-- scrolspy library -->
<script src="webroots/js/bootstrap-scrollspy.js"></script>
<!-- library for creating tabs -->
<script src="webroots/js/bootstrap-tab.js"></script>
<!-- library for advanced tooltip -->
<script src="webroots/js/bootstrap-tooltip.js"></script>
<!-- popover effect library -->
<script src="webroots/js/bootstrap-popover.js"></script>
<!-- button enhancer library -->
<script src="webroots/js/bootstrap-button.js"></script>
<!-- accordion library (optional, not used in demo) -->
<script src="webroots/js/bootstrap-collapse.js"></script>
<!-- carousel slideshow library (optional, not used in demo) -->
<script src="webroots/js/bootstrap-carousel.js"></script>
<!-- autocomplete library -->
<script src="webroots/js/bootstrap-typeahead.js"></script>
<!-- tour library -->
<script src="webroots/js/bootstrap-tour.js"></script>
<!-- library for cookie management -->
<script src="webroots/js/jquery.cookie.js"></script>
<!-- calander plugin -->
<script src='webroots/js/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='webroots/js/jquery.dataTables.min.js'></script>

<!-- chart libraries start -->
<script src="webroots/js/excanvas.js"></script>
<script src="webroots/js/jquery.flot.min.js"></script>
<script src="webroots/js/jquery.flot.pie.min.js"></script>
<script src="webroots/js/jquery.flot.stack.js"></script>
<script src="webroots/js/jquery.flot.resize.min.js"></script>
<!-- chart libraries end -->

<!-- select or dropdown enhancer -->
<script src="webroots/js/jquery.chosen.min.js"></script>
<!-- checkbox, radio, and file input styler -->
<script src="webroots/js/jquery.uniform.min.js"></script>
<!-- plugin for gallery image view -->
<script src="webroots/js/jquery.colorbox.min.js"></script>
<!-- rich text editor library -->
<script src="webroots/js/jquery.cleditor.min.js"></script>
<!-- notification plugin -->
<script src="webroots/js/jquery.noty.js"></script>
<!-- file manager library -->
<script src="webroots/js/jquery.elfinder.min.js"></script>
<!-- star rating plugin -->
<script src="webroots/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="webroots/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="webroots/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="webroots/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="webroots/js/jquery.history.js"></script>
<!-- Jquery validate pugin -->
<script src="webroots/js/jquery.validate.js"></script>

<script type="text/javascript" src="../libraries/ckeditor/ckeditor.js"></script>		


<!-- application script for Charisma demo -->
<script src="webroots/js/charisma.js"></script>
<script src="webroots/js/upload.js"></script>

