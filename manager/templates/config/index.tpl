<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> Role Detail</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<fieldset>
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <tbody>
					<tr>
						<td>Hotline</td>
						<td>{$value.phone}</td>
					</tr>
					<tr>
						<td>Title web</td>
						<td>{$value.web_title}</td>
					</tr>
					<tr>
						<td>Keyword seo</td>
						<td>{$value.web_keyword}</td>
					</tr>
					<tr>
						<td>Description seo</td>
						<td>{$value.web_description}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>{$value.description}</td>
					</tr>
				  </tbody>
			  </table>            


			</fieldset>
		
		</div>
	</div><!--/span-->

</div><!--/row-->
