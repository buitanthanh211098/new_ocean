<?php /* Smarty version Smarty-3.1.18, created on 2019-07-24 05:06:15
         compiled from "C:\xampp\htdocs\new_ocean\manager\templates\product\invoice_item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:140045d36cc2b0f8215-84166828%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a247f560d1ac933e45a3e6b8733b9ea617d619c5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\new_ocean\\manager\\templates\\product\\invoice_item.tpl',
      1 => 1563936900,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '140045d36cc2b0f8215-84166828',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5d36cc2b1713a7_09890860',
  'variables' => 
  array (
    'list' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d36cc2b1713a7_09890860')) {function content_5d36cc2b1713a7_09890860($_smarty_tpl) {?><div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> Danh sách sản phẩm</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>Id</th>
					  <th class="center">Hình ảnh</th>
					  <th>Tên sản phẩm</th>
					  <th class="center">Giá thanh toán</th>
					  <th class="center">Khuyến mại</th>
					  <th class="center">Số lượng</th>
					  <th class="center">Thành tiền</th>
					  <th class="center" width="80px">Xử lý</th>
				  </tr>
			  </thead>   
			  <tbody>
                <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['value']->value['product_id'];?>
</td>
					<td class="center" width="100px"><img alt="<?php echo $_smarty_tpl->tpl_vars['value']->value['image'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
" width="100px"></td>
					<td><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['price'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['promotions'];?>
 %</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['number'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['price_sale'];?>
</td>
					<td class="center">
						<a title="Delete" href="#" link="?mod=helps&site=delete&table=product_invoice_item&id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" class="confirmer"><i class="icon-trash"></i></a>
					</td>
				</tr>
                <?php } ?>
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->

</div><!--/row-->
<?php }} ?>
