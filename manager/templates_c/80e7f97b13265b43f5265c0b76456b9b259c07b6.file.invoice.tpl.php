<?php /* Smarty version Smarty-3.1.18, created on 2019-05-20 08:19:53
         compiled from "/home/customer/bnobn.com.vn/manager/templates/product/invoice.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13424081215ce200b94ae221-55641829%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80e7f97b13265b43f5265c0b76456b9b259c07b6' => 
    array (
      0 => '/home/customer/bnobn.com.vn/manager/templates/product/invoice.tpl',
      1 => 1407158390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13424081215ce200b94ae221-55641829',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list' => 0,
    'value' => 0,
    'list_handle' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5ce200b984ff21_40645451',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce200b984ff21_40645451')) {function content_5ce200b984ff21_40645451($_smarty_tpl) {?><div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-user"></i> Danh sách sản phẩm</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<form method="post" action="">
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
				      <th class="center"><input type="checkbox" class="checkall"></th>
					  <th>Id</th>
					  <th>Thời gian gửi</th>
					  <th>Người gửi</th>
					  <th class="center">Điện thoại</th>
					  <th class="center">Địa chỉ</th>
					  <th class="center">Email</th>
					  <th class="center">Giao hàng</th>
					  <th class="center">Trạng thái</th>
					  <th class="center" width="80px">Xử lý</th>
				  </tr>
			  </thead>   
			  <tbody>
                <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
				<tr>
					<td class="center"><input type="checkbox" name="check[]" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"></td>
					<td><?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['value']->value['created'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['value']->value['guest_name'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['guest_phone'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['guest_address'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['guest_email'];?>
</td>
					<td class="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['future'];?>
</td>
					<td class="center">
                        <a href="#" table="product_invoice" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" class="label frm_active <?php if ($_smarty_tpl->tpl_vars['value']->value['active']==1) {?>label-success<?php } else { ?>label-warning<?php }?>" title="Click to Active/Inactive  this filed"><?php echo $_smarty_tpl->tpl_vars['value']->value['active_view'];?>
</a>
					</td>
					<td class="center">
						<a title="View" href="product/invoice_item?id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><i class="icon-zoom-in"></i></a> &nbsp;&nbsp;|&nbsp;&nbsp; 
						<a title="Delete" href="#" link="?mod=helps&site=delete&table=product_invoice&id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" class="confirmer"><i class="icon-trash"></i></a>
					</td>
				</tr>
                <?php } ?>
			  </tbody>
			  <tfoot>
			  	<tr>
			  		<th colspan="10">
					<div class="controls">
					  <select id="" name="handle">
						<option value="0">Select</option>
						<?php echo $_smarty_tpl->tpl_vars['list_handle']->value;?>

					  </select>
					  <input type="submit" class="btn btn-primary" name="frmSubmit" value="Save changes"/>
					</div>
			  		</th>
			  	</tr>
			  </tfoot>
		  </table> 
		  </form>            
		</div>
	</div><!--/span-->

</div><!--/row-->
<?php }} ?>
