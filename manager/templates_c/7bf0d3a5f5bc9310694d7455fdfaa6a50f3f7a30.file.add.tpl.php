<?php /* Smarty version Smarty-3.1.18, created on 2016-11-07 05:41:55
         compiled from "/home/bnobn66/domains/bnobn.com.vn/public_html/manager/templates/user/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31281492858201423ef7266-13146771%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7bf0d3a5f5bc9310694d7455fdfaa6a50f3f7a30' => 
    array (
      0 => '/home/bnobn66/domains/bnobn.com.vn/public_html/manager/templates/user/add.tpl',
      1 => 1407158390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31281492858201423ef7266-13146771',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'roles' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_582014240aeb60_59926728',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_582014240aeb60_59926728')) {function content_582014240aeb60_59926728($_smarty_tpl) {?><div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> Form Create New user</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<form class="form-horizontal" action="" id="NewUser" method="post">
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="">Full Name</label>
					<div class="controls">
					  <input class="input-xlarge" name="name" type="text" required>
					</div>
				  </div>

				  <div class="control-group">
					<label class="control-label">username</label>
					<div class="controls">
					  <input class="input-xlarge required" name="username" type="text" >
					</div>
				  </div>

				  <div class="control-group">
					<label class="control-label" for="">Password</label>
					<div class="controls">
					  <input class="input-xlarge required" name="password" type="password" >
					</div>
				  </div>

				  <div class="control-group">
					<label class="control-label">Role</label>
					<div class="controls">
					  <select id="selectError3" name="role" class="required">
						<option value="">Select</option>
                            <?php echo $_smarty_tpl->tpl_vars['roles']->value;?>

					  </select>
					</div>
				  </div>
                           
				  <div class="control-group">
					<label class="control-label" for="">Email</label>
					<div class="controls">
					  <input class="input-xlarge" id="email" name="email" type="email" required>
					</div>
				  </div>

				  <div class="control-group">
					<label class="control-label" for="">Position</label>
					<div class="controls">
					  <input class="input-xlarge" name="position" type="text">
					</div>
				  </div>

				  <div class="form-actions">
					<button type="submit" class="btn btn-primary" name="FrmSubmit">Save changes</button>
					<button type="reset" class="btn">Cancel</button>
				  </div>
				</fieldset>
			  </form>
		
		</div>
	</div><!--/span-->

</div><!--/row-->
<?php }} ?>
