<?php /* Smarty version Smarty-3.1.18, created on 2019-01-24 09:27:28
         compiled from "/home/customer/bnobn.com.vn/manager/templates/article/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18935442785c4922901514f3-97779985%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '49987ea0b9bd7d5db356cb7bc42ffaaec87c1946' => 
    array (
      0 => '/home/customer/bnobn.com.vn/manager/templates/article/view.tpl',
      1 => 1407158388,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18935442785c4922901514f3-97779985',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang_title' => 0,
    'value' => 0,
    'lang_category' => 0,
    'lang_image' => 0,
    'lang_important' => 0,
    'lang_description' => 0,
    'lang_content' => 0,
    'lang_source' => 0,
    'lang_created' => 0,
    'lang_order' => 0,
    'lang_status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5c492290406f00_65162909',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c492290406f00_65162909')) {function content_5c492290406f00_65162909($_smarty_tpl) {?><div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> Article Detail</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<fieldset>
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <tbody>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_title']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_category']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['category'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_image']->value;?>
</td>
						<td class="center">
                           <ul class="thumbnails gallery">
                               <li id="" class="thumbnail">
                                   <a style="background:url(<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
)" title="<?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
">
                                       <img class="grayscale" src="<?php echo $_smarty_tpl->tpl_vars['value']->value['img'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
">
                                   </a>
                               </li>
                           </ul>
                       </td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_important']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['future'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_description']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['description'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_content']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['content'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_source']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['source'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_created']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['created'];?>
</td>
					</tr>
					<tr>
						<td>updated</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['updated'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_order']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['order_by'];?>
</td>
					</tr>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['lang_status']->value;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['value']->value['active'];?>
</td>
					</tr>
				  </tbody>
			  </table>            


			  <div class="form-actions">
				<a href="?mod=article&site=edit&id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" class="btn btn-primary">Update this field</a>
				<a href="?mod=article&site=manager" class="btn">Back to manager</a>
			  </div>
			</fieldset>
		
		</div>
	</div><!--/span-->

</div><!--/row-->
<?php }} ?>
