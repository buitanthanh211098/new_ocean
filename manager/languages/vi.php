<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng viet
 * 
 */

$smarty->assign('lang_create', 'Thêm mới');
$smarty->assign('lang_manage', 'Quản lý');
$smarty->assign('lang_edit', 'Chỉnh sửa');
$smarty->assign('lang_delete', 'Xoá');
$smarty->assign('lang_detail', 'Thông tin');
$smarty->assign('lang_save', 'Lưu lại');
$smarty->assign('lang_cancel', 'Huỷ bỏ');

$smarty->assign('lang_id', 'Mã');
$smarty->assign('lang_name', 'Tên');
$smarty->assign('lang_title', 'Tiêu đề');
$smarty->assign('lang_important', 'Quan trọng');
$smarty->assign('lang_image', 'Hình ảnh');
$smarty->assign('lang_category', 'Danh mục');
$smarty->assign('lang_parent', 'Danh mục cha');
$smarty->assign('lang_description', 'Thông tin mô tả');
$smarty->assign('lang_content', 'Nội dung');
$smarty->assign('lang_order', 'Thứ tự');
$smarty->assign('lang_source', 'Nguồn tin');
$smarty->assign('lang_price', 'Giá bán');
$smarty->assign('lang_province', 'Tỉnh thành');
$smarty->assign('lang_district', 'Quận huyện');
$smarty->assign('lang_future', 'Quan trọng');
$smarty->assign('lang_created', 'Ngày tạo');
$smarty->assign('lang_status', 'Trạng thái');
$smarty->assign('lang_action', 'Thực hiện');
$smarty->assign('lang_role', 'Quyền hạn');
$smarty->assign('lang_select', 'Lựa chọn');
$smarty->assign('lang_position', 'Vị trí hiển thị');
$smarty->assign('lang_link', 'Đường dẫn');
$smarty->assign('lang_code', 'Mã hiển thị');
$smarty->assign('lang_promotions', 'Khuyến mại');
$smarty->assign('lang_keyseo', 'Từ khoá seo web');
$smarty->assign('lang_desseo', 'Mô tả seo web');
$smarty->assign('lang_warranty', 'Bảo hành');
$smarty->assign('lang_maker', 'Nhà sản xuất');
$smarty->assign('lang_alias', 'Tên url');
$smarty->assign('lang_module', 'Mudule hiển thị');
$smarty->assign('lang_type', 'Kiểu');


$smarty->assign('lang_form_create', 'Thêm mới');



$smarty->assign('lang_home', 'Trang chủ');
$smarty->assign('lang_configuration', 'Thiết lập cấu hình');
$smarty->assign('lang_user', 'Users');
$smarty->assign('lang_menu', 'Menu');
$smarty->assign('lang_gallery', 'Hình ảnh');
$smarty->assign('lang_article', 'Bài viết');
$smarty->assign('lang_product', 'Sản phẩm');
$smarty->assign('lang_contact', 'Liên hệ');
$smarty->assign('lang_album', 'Album ảnh');



