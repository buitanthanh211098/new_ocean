<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng anh
 * 
 */

$smarty->assign('lang_create', 'Create');
$smarty->assign('lang_manage', 'Manager');
$smarty->assign('lang_edit', 'Manager');
$smarty->assign('lang_delete', 'Delete');
$smarty->assign('lang_save', 'Save changes');
$smarty->assign('lang_cancel', 'Cancel');

$smarty->assign('lang_name', 'Name');
$smarty->assign('lang_title', 'Title');
$smarty->assign('lang_important', 'Important');
$smarty->assign('lang_image', 'Image Upload');
$smarty->assign('lang_category', 'Category');
$smarty->assign('lang_parent', 'Parent');
$smarty->assign('lang_description', 'Description');
$smarty->assign('lang_content', 'Content');
$smarty->assign('lang_order', 'Order');
$smarty->assign('lang_source', 'Source');
$smarty->assign('lang_price', 'Price');
$smarty->assign('lang_province', 'Province');
$smarty->assign('lang_district', 'District');
$smarty->assign('lang_future', 'District');
$smarty->assign('lang_created', 'District');
$smarty->assign('lang_status', 'District');
$smarty->assign('lang_action', 'District');
$smarty->assign('lang_role', 'Role');
$smarty->assign('lang_select', 'Select Option');
$smarty->assign('lang_position', 'Position');

$smarty->assign('lang_form_create', 'Create new');
$smarty->assign('lang_district', 'District');



$smarty->assign('lang_home', 'Home');
$smarty->assign('lang_configuration', 'Configuration');
$smarty->assign('lang_user', 'Users');
$smarty->assign('lang_menu', 'Menus');
$smarty->assign('lang_gallery', 'Galleries');
$smarty->assign('lang_article', 'Articlies');
$smarty->assign('lang_product', 'Products');
$smarty->assign('lang_contact', 'Contacts');
$smarty->assign('lang_album', 'Album');



