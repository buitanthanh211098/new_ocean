<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu Tieng Nhat
 */

# Cac tu dung chung
$trans['products'] = "Products";
$trans['product_category'] = "Product Categories";
$trans['product_new'] = "New Products";
$trans['product_hot'] = "Hot Products";
$trans['product_other'] = "Other Products";
$trans['product_search'] = "Search for products";
$trans['product_cart'] = "Shopping Cart";
$trans['product_add_cart'] = "Add to cart";
$trans['product_price'] = "Price";
$trans['product_info'] = "Product information";
$trans['product_none'] = "Empty product";

$trans['news'] = "ニュース";
$trans['news_category'] = "News Categories";
$trans['news_hot'] = "Hot News";
$trans['news_new'] = "ニュース";
$trans['news_other'] = "Other News";
$trans['news_none'] = "空のニュース";

$trans['gen_intro'] = "Introduction";
$trans['gen_info'] = "Information";
$trans['gen_support'] = "Support";
$trans['gen_statistic'] = "Counter Statistics";
$trans['gen_key_search'] = "Keyword search";
$trans['gen_partner'] = "Partners";

$trans['gen_view'] = "詳細";

$trans['contact'] = "連絡";
$trans['contact_info'] = "連絡先情報";
$trans['contact_name'] = "連絡先";
$trans['contact_phone'] = "電話番号";
$trans['contact_email'] = "Eメール";
$trans['contact_title'] = "テーマ";
$trans['contact_address'] = "アドレス";
$trans['contact_company'] = "会社名";
$trans['contact_content'] = "コンテンツ";
$trans['contact_send'] = "連絡先を送信";
$trans['contact_wellcome'] = "ここに私達に連絡し送信する";
$trans['contact_description'] = "下記のフォームに完全な連絡先の詳細を入力してください、私たちはできるだけ早くあなたに応答します。";

$trans["gen_develop"] = "Develop by";

$smarty->assign('trans', $trans);
