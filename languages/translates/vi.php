<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng viet
 * 
 */

# Cac tu dung chung
$trans['products'] = "Danh sách sản phẩm";
$trans['product_category'] = "Danh mục sản phẩm";
$trans['product_new'] = "Sản phẩm mới";
$trans['product_hot'] = "Sản phẩm nổi bật";
$trans['product_other'] = "Sản phẩm liên quan";
$trans['product_search'] = "Tìm kiếm sản phẩm";
$trans['product_cart'] = "Giỏ hàng";
$trans['product_add_cart'] = "Đặt hàng";
$trans['product_price'] = "Giá";
$trans['product_info'] = "Thông tin sản phẩm";
$trans['product_none'] = "Không có sản phẩm";

$trans['news'] = "Danh sách tin tức";
$trans['news_category'] = "Danh mục tin tức";
$trans['news_hot'] = "Tin nổi bật";
$trans['news_new'] = "Tin mới";
$trans['news_other'] = "Tin liên quan";
$trans['news_none'] = "Không có bài viết";

$trans['gen_intro'] = "Giới thiệu";
$trans['gen_info'] = "Thông tin";
$trans['gen_support'] = "Hỗ trợ trực tuyến";
$trans['gen_statistic'] = "Thống kê truy cập";
$trans['gen_key_search'] = "Từ khoá tìm kiếm";
$trans['gen_partner'] = "Đối tác";

$trans['gen_view'] = "Chi tiết";

$trans['contact'] = "Liên hệ";
$trans['contact_info'] = "Thông tin liên hệ";
$trans['contact_name'] = "Tên của bạn";
$trans['contact_phone'] = "Số điện thoại";
$trans['contact_email'] = "Địa chỉ email";
$trans['contact_content'] = "Nội dung";
$trans['contact_send'] = "Gửi liên hệ";
$trans['contact_wellcome'] = "Gửi liên hệ tới chúng tôi tại đây";
$trans['contact_description'] = "Vui lòng nhập đầy đủ thông tin liên hệ vào form bên dưới, chúng tôi sẽ phản hồi lại bạn trong thời gian sớm nhất.";

$trans["gen_develop"] = "Thiết kế website";
$trans["gen_home"] = "Trang chủ";

$smarty->assign('trans', $trans);
