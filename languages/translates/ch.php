<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng Anh
 * 
 */

$smarty->assign('lang_home_search', '产品搜索');
$smarty->assign('lang_home_category', '产品类别');
$smarty->assign('lang_home_support', '支持');
$smarty->assign('lang_home_weather', '天气');
$smarty->assign('lang_home_statistics', '统计');
$smarty->assign('lang_home_hotnews', '热点新闻');
$smarty->assign('lang_home_partner', '伙伴');
$smarty->assign('lang_home_intro', '介绍');
$smarty->assign('lang_home_hotproduct', '热门产品');

$smarty->assign('lang_home_key', '关键词');
$smarty->assign('lang_home_view', '查看详细');
$smarty->assign('lang_home_image', '图像');
$smarty->assign('lang_home_detail', '细节');
$smarty->assign('lang_home_otherpro', '其他产品');
$smarty->assign('lang_home_othernews', '其他新闻');
$smarty->assign('lang_home_price', '价格');

$lang_product = "产品列表";
$smarty->assign('lang_home_product', $lang_product);
$smarty->assign('lang_home_contact', '联系');
$smarty->assign('lang_home_contact_intro', '联系介绍');
$smarty->assign('lang_home_contact_send', '发送联系');
$smarty->assign('lang_home_contact_required', '请输入所有信息');

$smarty->assign('lang_home_contact_name', '全名');
$smarty->assign('lang_home_contact_phone', '电话号码');
$smarty->assign('lang_home_contact_email', '电子邮件');
$smarty->assign('lang_home_contact_address', '地址');
$smarty->assign('lang_home_contact_title', '标题');
$smarty->assign('lang_home_contact_content', '内容');


