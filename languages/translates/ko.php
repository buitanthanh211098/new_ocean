<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng Anh
 * 
 */

$smarty->assign('lang_home_search', '제품 검색');
$smarty->assign('lang_home_category', '제품 카테고리');
$smarty->assign('lang_home_support', '지원');
$smarty->assign('lang_home_weather', '날씨');
$smarty->assign('lang_home_statistics', '통계');
$smarty->assign('lang_home_hotnews', '최신 뉴스');
$smarty->assign('lang_home_partner', '파트너');
$smarty->assign('lang_home_intro', '소개');
$smarty->assign('lang_home_hotproduct', '핫 제품');

$smarty->assign('lang_home_key', '키워드');
$smarty->assign('lang_home_view', '상세보기');
$smarty->assign('lang_home_image', '영상');
$smarty->assign('lang_home_detail', '세부 정보');
$smarty->assign('lang_home_otherpro', '기타 제품');
$smarty->assign('lang_home_othernews', '기타 뉴스');
$smarty->assign('lang_home_price', '가격');

$lang_product = "제품 목록";
$smarty->assign('lang_home_product', $lang_product);
$smarty->assign('lang_home_contact', '접촉');
$smarty->assign('lang_home_contact_intro', '소개 연락처');
$smarty->assign('lang_home_contact_send', '문의 보내기');
$smarty->assign('lang_home_contact_required', '모든 정보를 입력하십시오');

$smarty->assign('lang_home_contact_name', '전체 이름');
$smarty->assign('lang_home_contact_phone', '전화 번호');
$smarty->assign('lang_home_contact_email', '이메일');
$smarty->assign('lang_home_contact_address', '주소');
$smarty->assign('lang_home_contact_title', '이름');
$smarty->assign('lang_home_contact_content', '만족');


