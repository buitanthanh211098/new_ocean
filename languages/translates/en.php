<?php
/*
 * Thiet lap ngon ngu cho site
 * Ngon ngu tieng anh
 * 
 */

# Cac tu dung chung
$trans['products'] = "Products";
$trans['product_category'] = "Product Categories";
$trans['product_new'] = "New Products";
$trans['product_hot'] = "Hot Products";
$trans['product_other'] = "Other Products";
$trans['product_search'] = "Search for products";
$trans['product_cart'] = "Shopping Cart";
$trans['product_add_cart'] = "Add to cart";
$trans['product_price'] = "Price";
$trans['product_info'] = "Product information";
$trans['product_none'] = "Empty product";

$trans['news'] = "News";
$trans['news_category'] = "News Categories";
$trans['news_hot'] = "Hot News";
$trans['news_new'] = "News";
$trans['news_other'] = "Other News";
$trans['news_none'] = "Empty news";

$trans['gen_intro'] = "Introduction";
$trans['gen_info'] = "Information";
$trans['gen_support'] = "Support";
$trans['gen_statistic'] = "Counter Statistics";
$trans['gen_key_search'] = "Keyword search";
$trans['gen_partner'] = "Partners";

$trans['gen_view'] = "Read this article";
$trans['gen_contact'] = "Contact";
$trans['gen_contact_info'] = "Contact information";

$trans['contact'] = "Contact";
$trans['contact_info'] = "Contact information";
$trans['contact_name'] = "Contact name";
$trans['contact_phone'] = "Tel";
$trans['contact_email'] = "Email";
$trans['contact_title'] = "Subject";
$trans['contact_address'] = "Address";
$trans['contact_company'] = "Company Name";
$trans['contact_content'] = "Content";
$trans['contact_send'] = "Send contact";
$trans['contact_wellcome'] = "Send contact us here";
$trans['contact_description'] = "Please enter full contact details in the form below, we will respond to you as soon as possible.";

$trans["gen_develop"] = "Develop by";

$smarty->assign('trans', $trans);
