<?php

# config danh sach phan quyen admin
$conf_user_role = array(
		1 => 'Guest',
		2 => 'User',
		3 => 'Manager',
		4 => 'Admin',
		5 => 'Super Admin',
		10 => 'Super'
);

$conf_position = array(
		0 => 'No position',
		1 => 'Position 1',
		2 => 'Position 2',
		3 => 'Position 3',
		4 => 'Position 4',
		5 => 'Position 5',
		6 => 'Position 6',
		7 => 'Position 7',
		8 => 'Position 8',
		9 => 'Position 9',
		10 =>'Position 10',
		11 => 'Position 11',
		12 => 'Position 12',
	    13 =>'Position 13',
		14 => 'Position 14',
		15 => 'Position 15',
		16 => 'Position 16',
);

$conf_gender = array(
		1 => 'Nữ',
		2 => 'Nam',
		3 => 'Khác'
);

$conf_multimedia_type = array(
		1 => 'Hình ảnh',
		2 => 'Flash',
		3 => 'Files'
);

$conf_language = array(
		1 => 'vi',
		2 => 'en',
		#3 => 'fr',
		#4 => 'ch',
		#5 => 'ko',
		3 => 'ja'
);
$conf_language_name = array(
		1 => array('id' => 'vi', 'name' => 'Tiếng Việt'),
		#2 => array('id' => 'en', 'name' => 'English'),
		#3 => array('id' => 'ja', 'name' => 'Japanese'),
);



$conf_product_level = array(
		1 => '1 sao',
		2 => '2 sao',
		3 => '3 sao',
		4 => '4 sao',
		5 => '5 sao'
);

$conf_product_warranty = array(
		1 => "1 tháng",
		2 => "3 tháng",
		3 => "6 tháng",
		4 => "12 tháng",
		5 => "18 tháng",
		6 => "24 tháng",
		7 => "36 tháng",
		8 => "60 tháng"
);


$conf_handle = array(
		1 => 'Active',
		2 => 'Huỷ active',
		3 => 'Quan trọng',
		4 => 'Không quan trọng',
		5 => 'Xoá',
		6 => 'Reset sắp xếp'
);

$conf_mail_type = array(
		1 => 'Email cá nhân',
		2 => 'Email công ty',
		3 => 'Email các tổ chức',
		4 => 'Khác'
);